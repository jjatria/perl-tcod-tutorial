# Perl TCOD Tutorial

This is an ongoing attempt to complete the tutorials in from [Roguelike Tutorials]
using the FFI bindings for Perl.

They aim to follow the tutorials in spirit, but doing what makes Perlish sense
using modern Perl features and tools.

To run, first install a version of `libtcod` on your machine. Then it should be
a matter of runing

    cpanm --installdeps . # only the first time, to install the dependencies
    ./bin/game

[Roguelike Tutorials]: https://rogueliketutorials.com
