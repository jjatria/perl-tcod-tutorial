package Game;

use strict;
use warnings;
use feature 'state';
use experimental 'signatures';

use Game::Entities;
use List::Util;
use TCOD;

use lib 'lib';
use Map;
use Messages;

# Game states
use constant {
    PLAYER_DEAD  => -1,
    PLAYERS_TURN =>  0,
    ENEMY_TURN   =>  1,

    LEVEL_CORPSE => -1,
    LEVEL_ITEM   =>  0,
    LEVEL_ACTOR  =>  1,
};

my ( $config, $log, $map, $ecs );

sub init (%args) {
    $config = { %args };

    my %map_init = (
        max_rooms    => delete $args{map}{max_rooms},
        max_size     => delete $args{map}{max_size},
        min_size     => delete $args{map}{min_size},
        max_monsters => delete $args{map}{max_monsters},
    );

    $ecs = Game::Entities->new;
    $map = Map->new( %{ $args{map} } )->make_map(%map_init);
    $log = MessageLog->new( %{ delete $args{ui}{log} // {} } );

    TCOD::Sys::set_fps( $args{fps} ) if $args{fps};

    TCOD::Console::set_custom_font(
        $args{font},
        TCOD::FONT_TYPE_GREYSCALE | TCOD::FONT_LAYOUT_TCOD,
    );

    TCOD::Console::init_root(
        @args{qw( width height )},
        'libtcod tutorial',
        0,
    );

    return;
}

sub solid_entity_at ( $x, $y ) {
    my $target = List::Util::first {
        my ( $guid, $body ) = ( $_->[0], @{ $_->[1] } );
        $body->solid && $body->x == $x && $body->y == $y;
    } @{ $ecs->view('C::Body') };

    $target->[0] if $target;
}

sub Log    { $log    }
sub Map    { $map    }
sub ECS    { $ecs    }
sub Config { $config }

{
    my $state = PLAYERS_TURN;

    sub state     { $state }
    sub end_game  { $state = PLAYER_DEAD }
    sub next_turn {
        return if $state == PLAYER_DEAD;
        $state = 1 - $state;
    }
}

{
    my @events;

    sub flush_events { @events = ()        }
    sub push_event   { push @events, shift }
    sub get_event    { shift @events       }
}

1;
