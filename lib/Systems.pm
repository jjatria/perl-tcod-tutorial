package Systems;

use strict;
use warnings;
use feature qw( say state );
use experimental 'signatures';

use POSIX 'lround';
use TCOD;

use lib 'lib';
use Game;
use Messages;

my $key   = TCOD::Key->new;
my $mouse = TCOD::Mouse->new;

sub input {
    TCOD::Sys::check_for_event(
        TCOD::EVENT_KEY_PRESS | TCOD::EVENT_MOUSE,
        $key, $mouse,
    );

    my $c = $key->c;
    for ( $key->vk ) {
        return if $_ == TCOD::K_NONE;
        return { move => [  0, -1 ] } if $c eq 'k' || $_ == TCOD::K_UP;
        return { move => [  0,  1 ] } if $c eq 'j' || $_ == TCOD::K_DOWN;
        return { move => [ -1,  0 ] } if $c eq 'h' || $_ == TCOD::K_LEFT;
        return { move => [  1,  0 ] } if $c eq 'l' || $_ == TCOD::K_RIGHT;
        return { move => [ -1, -1 ] } if $c eq 'y';
        return { move => [  1, -1 ] } if $c eq 'u';
        return { move => [ -1,  1 ] } if $c eq 'b';
        return { move => [  1,  1 ] } if $c eq 'n';

        return { fullscreen => 1 } if $_ == TCOD::K_ENTER;

        return { exit => 1 } if $_ == TCOD::K_ESCAPE;
    }

    return;
}

sub render ( $console ) {
    state $panel = TCOD::Console->new(
        Game::Map->width,
        $console->get_height - Game::Map->height,
    );

    state $panel_y = $console->get_height - $panel->get_height;

    Game::Map->render($console);

    my @mouse_names;
    Game::ECS->view(qw( C::Body C::Draw ))->each(
        sub ( $, $body, $draw ) {
            return unless Game::Map->is_visible( $body->xy );
            $console->set_default_foreground( $draw->color );
            $console->put_char( $body->xy, $draw->char, TCOD::BKGND_NONE );

            return unless $body->x == $mouse->cx && $body->y == $mouse->cy;
            push @mouse_names, $body->name;
        }
    );

    $console->blit( 0, 0, 0, 0, undef, 0, 0, 1, 1 );

    Game::ECS->view('C::Body')->each( sub ( $, $body ) {
        $console->put_char( $body->xy, ord(' '), TCOD::BKGND_NONE );
    });

    $panel->set_default_background( TCOD::BLACK );
    $panel->clear;

    if ( @mouse_names ) {
        $panel->set_default_foreground( TCOD::GREY );
        $panel->print_ex(
            1, 0, TCOD::BKGND_NONE, TCOD::LEFT,
            ucfirst join ', ', @mouse_names,
        );
    }

    Game::ECS->view(qw( C::Player C::Fighter ))->each( sub ( $, $, $fighter ) {
        state $width = Game::Config->{ui}{health_bar}{width};

        render_bar( $panel, 1, 1, $width, 'HP',
            $fighter->hp, $fighter->max_hp, TCOD::LIGHT_RED, TCOD::DARK_RED );
    });

    my $y;
    for ( Game::Log->messages ) {
        $panel->set_default_background( $_->color );
        $panel->print_ex( Game::Log->x, ++$y, TCOD::BKGND_NONE, TCOD::LEFT, $_->text );
    }

    $panel->blit( 0, 0, 0, 0, undef, 0, $panel_y, 1, 1 );

    TCOD::Console::flush;
}

sub render_bar ( $console, $x, $y, $total_width, $name, $value, $maximum, $color, $bg ) {
    my $width = int $value / $maximum * $total_width;

    $console->set_default_background( $bg );
    $console->rect( $x, $y, $total_width, 1, 0, TCOD::BKGND_SCREEN );

    $console->set_default_background( $color );
    $console->rect( $x, $y, $width, 1, 0, TCOD::BKGND_SCREEN ) if $width > 0;

    $console->set_default_foreground( TCOD::WHITE );
    $console->print_ex(
        int( $x + $total_width / 2), $y, TCOD::BKGND_NONE, TCOD::CENTER,
        sprintf '%s: %s/%s', $name, $value, $maximum,
    );
}

my sub move_towards ( $self, $other ) {
    my $dx = $other->x - $self->x;
    my $dy = $other->y - $self->y;

    my $distance = sqrt $dx ** 2 + $dy ** 2;

    $dx = lround $dx / $distance;
    $dy = lround $dy / $distance;

    my $xx = $self->x + $dx;
    my $yy = $self->y + $dy;

    return if Game::Map->is_solid( $xx, $yy )
        || Game::solid_entity_at( $xx, $yy );

    $self->x += $dx;
    $self->y += $dy;
}

my sub move_astar ( $self, $other ) {
    # Copy game map for pathfinding
    my $fov = Game::Map->copy;

    # Mark solid entities as non-walkable so we can navigate around them.
    # This will only be called when we are not next to the target.
    Game::ECS->view('C::Body')->each( sub ( $guid, $body ) {
        return if !$body->solid
            || ( $body->x == $self->x  && $body->y == $self->y  )
            || ( $body->x == $other->x && $body->y == $other->y );

        $fov->set_properties( $body->xy, 1, 0 );
    });

    # Allocate a A* path
    my $path = TCOD::Path->new_using_map( $fov, 1.41 );

    # Compute the path between self's coordinates and the other's coordinates
    $path->compute( $self->xy, $other->xy );

    # We cap the path size so we don't send entities running to the other side
    # of the map by acident.
    if ( !$path->is_empty && $path->size < 25 ) {
        my ( $x, $y ) = $path->walk;
        if ( $x || $y ) {
            $self->x = $x;
            $self->y = $y;
        }
    }
    else {
        # Keep the old move function as a backup so that if there are no paths
        # (for example another monster blocks a corridor) it will still try to
        # move towards the player (closer to the corridor opening)
        move_towards( $self, $other );
    }
}

my sub attack ( $self, $other ) {
    my ( $off, $off_body ) = Game::ECS->get( $self  => qw( C::Fighter C::Body ));
    my ( $def, $def_body ) = Game::ECS->get( $other => qw( C::Fighter C::Body ));

    if ( my $damage = $off->power - $def->defense  > 0 ) {
        my $message = sprintf '%s attacks %s for %d hit point%s',
            ucfirst $off_body->name, ucfirst $def_body->name, $damage,
            ( $damage == 1 ? '' : 's' );

        Game::push_event({ message => Message->new( $message ) });

        $def->hp -= $damage;
        Game::push_event({ dead => $other }) if $def->hp <= 0;
    }
    else {
        my $message = sprintf '%s attacks %s but does no damage.',
            $off_body->name, $def_body->name;

        Game::push_event({ message => Message->new( $message ) });
    }
}

sub player ( $player, $delta ) {
    my $redraw;

    my ( $body, $fighter )
        = Game::ECS->get( $player => qw( C::Body C::Fighter ) );

    die 'Player has no body!'   unless $body;
    die 'Player is no fighter!' unless $fighter;

    my ( $dx, $dy, $x, $y ) = ( @$delta, $body->xy );
    my ( $xx, $yy ) = ( $x + $dx, $y + $dy );

    return if Game::Map->is_solid( $xx, $yy );

    if ( my $guid = Game::solid_entity_at( $xx, $yy ) ) {
        attack( $player, $guid );
    }
    else {
        $body->move( $dx, $dy );
        $redraw = 1;
    }

    Game::next_turn;
    return $redraw;
}

sub monsters ( $player ) {
    my ( $player_body, $player_fighter )
        = Game::ECS->get( $player => qw( C::Body C::Fighter ) );

    die 'Player has no body!'   unless $player_body;
    die 'Player is no fighter!' unless $player_fighter;

    Game::ECS->view(qw( C::Monster C::Body C::Fighter ))->each(
        sub ( $guid, $, $body, $fighter ) {
            return unless Game::Map->is_visible( $body->xy );

            if ( $body->distance_to( $player_body->xy ) >= 2 ) {
                move_astar( $body, $player_body );
            }
            elsif ( $player_fighter->hp > 0 ) {
                attack( $guid, $player );
            }
        }
    );

    Game::next_turn;
}

my sub kill_entity ( $guid ) {
    my $draw = Game::ECS->get( $guid => 'C::Draw' )
        or die 'Dead entity is not drawable!';

    $draw->char  = ord '%';
    $draw->color = TCOD::DARK_RED;
    $draw->level = Game::LEVEL_CORPSE;

    if ( Game::ECS->check( $guid => 'C::Player' ) ) {
        Game::flush_events;
        Game::push_event({ message => Message->new( 'You died!' ) });
        Game::end_game;
        return;
    }

    if ( Game::ECS->check( $guid => 'C::Monster' ) ) {
        my $body = Game::ECS->get( $guid => 'C::Body' );

        Game::push_event({
            message => Message->new( $body->name . ' is dead!' ),
        });

        $body->solid = 0;
        $body->name = 'remains of ' . $body->name;

        Game::ECS->delete( $guid => qw( C::Fighter ));
    }

    Game::ECS->sort( 'C::Draw' => sub { $a->level <=> $b->level } );
    Game::ECS->sort( 'C::Body' => 'C::Draw' );
}

sub process_events {
    while ( my $event = Game::get_event ) {
        Game::Log->add_message( $event->{message} ) if $event->{message};

        if ( defined $event->{dead} ) {
            kill_entity( $event->{dead} );
        }
    }
}

1;
