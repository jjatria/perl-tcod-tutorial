use Object::Pad;

use strict;
use warnings;

class C::Player { }

class C::Monster { }

class C::Body {
    has $name   :param :mutator;
    has $x      :param :mutator;
    has $y      :param :mutator;
    has $solid  :param :mutator = 1;
    has $opaque :param :reader  = 1;

    sub BUILDARGS ( $class, $x, $y, $name ){
        ( x => $x, y => $y, name => $name );
    }

    method xy { ( $x, $y ) }

    method move ( $dx, $dy ) {
        $x += $dx;
        $y += $dy;
    }

    method distance_to ( $target_x, $target_y ) {
        my $dx = $target_x - $x;
        my $dy = $target_y - $y;

        sqrt $dx ** 2 + $dy ** 2;
    }
}

class C::Draw {
    has $char  :param :mutator;
    has $color :param :mutator;
    has $level :param :mutator = 1;

    sub BUILDARGS ( $class, $char, $color ){
        ( char => ord($char), color => $color );
    }
}

class C::Fighter {
    has $max_hp  :param :reader = 0;
    has $hp      :param :mutator;
    has $defense :param :reader;
    has $power   :param :reader;

    BUILD {
        $max_hp = $hp;
    }
}
