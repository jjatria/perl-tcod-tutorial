use Object::Pad;

use strict;
use warnings;

use TCOD;
use List::Util;

use lib 'lib';
use Components;
use Game;
use Rect;
use Tile;

class Map {
    has $width  :reader :param;
    has $height :reader :param;
    has $colors         :param;
    has $tiles  :reader = [];
    has $spawn_point :reader = [ 0, 0 ];
    has $fov;
    has $dirty = 1;

    BUILD {
        $tiles = [
            map [
                map Tile->new( solid => 1 ), 0 .. $height
            ], 0 .. $width
        ];
    }

    sub randint ( $lo, $hi ) { $lo + int rand $hi - $lo + 1 }

    method is_solid ( $x, $y ) {
        return 1 if $x < 0 || $y < 0 || $x >= $width || $y >= $height;
        $self->tiles->[$x][$y]->solid;
    }

    method is_visible ( $x, $y ) { $fov->is_in_fov( $x, $y ) }

    method refresh ( $x, $y, $radius, $light_walls = 1, $algo = TCOD::FOV_BASIC ) {
        $fov->compute_fov( $x, $y, $radius, $light_walls, $algo );
        $dirty = 1;
    }

    method copy {
        my $copy = TCOD::Map->new( $width, $height );
        $fov->copy( $copy );
        $copy;
    }

    method render ( $console ) {
        return unless $dirty;

        $dirty = 0;
        for my $x ( 0 .. $width - 1 ) {
            my $col = $tiles->[$x];

            for my $y ( 0 .. $height - 1 ) {
                my $opaque  = $col->[$y]->opaque;
                my $visible = $fov->is_in_fov( $x, $y );

                my $color;
                if ($visible) {
                    $color = $colors->{ $opaque ? 'light_wall' : 'light_ground' };
                }
                else {
                    $color = $colors->{ $opaque ? 'dark_wall' : 'dark_ground' };
                }

                $console->set_char_background( $x, $y, $color, TCOD::BKGND_SET );
            }
        }
    }

    method make_map ( %args ) {
        my @rooms;
        my $con = TCOD::Console->new( $width, $height );

        for my $r ( 0 .. $args{max_rooms} - 1 ) {
            my $w = randint $args{min_size}, $args{max_size};
            my $h = randint $args{min_size}, $args{max_size};

            my $x = randint 0, $width  - $w - 1;
            my $y = randint 0, $height - $h - 1;

            my $room = Rect->new( $x, $y, $w, $h );

            next if List::Util::any { $room->intersects($_) } @rooms;

            $self->create_room( $room );

            my ( $new_x, $new_y ) = $room->center;

            if ( !@rooms) {
                # First room is the player spawn point
                $spawn_point = [ $new_x, $new_y ];
            }
            else {
                my ( $prev_x, $prev_y ) = $rooms[-1]->center;

                if ( int rand ) {
                    $self->create_h_tunnel( $prev_x, $new_x, $prev_y );
                    $self->create_v_tunnel( $prev_y, $new_y, $new_x );
                }
                else {
                    $self->create_v_tunnel( $prev_y, $new_y, $prev_x );
                    $self->create_h_tunnel( $prev_x, $new_x, $new_y );
                }
            }

            $self->place_entities( $room, $args{max_monsters} );
            push @rooms, $room;
        }

        $fov = TCOD::Map->new( $width, $height );

        for my $x ( 0 .. $width - 1 ) {
            my $col = $tiles->[$x];

            for my $y ( 0 .. $height - 1 ) {
                $fov->set_properties(
                    $x, $y,
                    !$col->[$y]->opaque,
                    !$col->[$y]->solid,
                )
            }
        }

        Game::ECS()->sort( 'C::Draw' => sub { $a->level <=> $b->level } );
        Game::ECS()->sort( 'C::Body' => 'C::Draw' );

        $self;
    }

    method create_room ( $room ) {
        for my $x ( $room->x1 + 1 .. $room->x2 - 1 ) {
            my $col = $tiles->[$x];

            for my $y ( $room->y1 + 1 .. $room->y2 - 1 ) {
                $col->[$y]->solid  = 0;
                $col->[$y]->opaque = 0;
            }
        }
    }

    method create_h_tunnel ( $start, $end, $y ) {
        ( $start, $end ) = ( $end, $start ) if $end < $start;

        for my $x ( $start .. $end ) {
            my $tile = $tiles->[$x][$y];

            $tile->opaque = 0;
            $tile->solid  = 0;
        }
    }

    method create_v_tunnel ( $start, $end, $x ) {
        ( $start, $end ) = ( $end, $start ) if $end < $start;

        for my $y ( $start .. $end ) {
            my $tile = $tiles->[$x][$y];

            $tile->opaque = 0;
            $tile->solid  = 0;
        }
    }

    method place_entities ( $room, $max_monsters ) {
        for ( 1 .. randint 0, $max_monsters ) {
            my $x = randint $room->x1 + 1, $room->x2 - 1;
            my $y = randint $room->y1 + 1, $room->y2 - 1;

            next if $x == $spawn_point->[0] && $y == $spawn_point->[1];
            next if List::Util::any {
                my ($body) = @$_;
                $body->x == $x && $body->y == $y
            } Game::ECS()->view('C::Body')->components;

            if ( rand() < .8 ) {
                Game::ECS()->create(
                    C::Monster->new,
                    C::Body->new( $x, $y, 'orc' ),
                    C::Draw->new( 'o', TCOD::DESATURATED_GREEN ),
                    C::Fighter->new( hp => 10, defense => 0, power => 3 ),
                );
            }
            else {
                Game::ECS()->create(
                    C::Monster->new,
                    C::Body->new( $x, $y, 'troll' ),
                    C::Draw->new( 'T', TCOD::GREEN ),
                    C::Fighter->new( hp => 16, defense => 1, power => 4 ),
                );
            }
        }
    }
}
