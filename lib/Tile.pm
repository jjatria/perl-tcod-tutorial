use Object::Pad;

use strict;
use warnings;

class Tile {
    has $solid  :mutator :param;
    has $opaque :mutator :param = 0;

    BUILD { $opaque ||= $solid }
}
