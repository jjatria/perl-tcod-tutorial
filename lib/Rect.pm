use Object::Pad;

use strict;
use warnings;

class Rect {
    has $x1 :param :reader;
    has $y1 :param :reader;
    has $x2 :param :reader;
    has $y2 :param :reader;

    sub BUILDARGS ( $class, $x, $y, $w, $h ) {
        ( x1 => $x, y1 => $y, x2 => $x + $w, y2 => $y + $h )
    }

    method center {
        ( int( ( $x1 + $x2 ) / 2 ), int( ( $y1 + $y2 ) / 2 ) )
    }

    method intersects ( $other ) {
           $x1 <= $other->x2
        && $x2 >= $other->x1
        && $y1 <= $other->y2
        && $y2 >= $other->y1;
    }
}
