use Object::Pad;

use strict;
use warnings;

use TCOD;

class Message {
    has $text  :param :reader;
    has $color :param :reader;

    sub BUILDARGS ( $class, $text, $color = TCOD::WHITE ) {
        ( text => $text, color => $color );
    }
}

class MessageLog {
    use Text::Wrap ();

    has $messages :param = [];
    has $width    :param;
    has $height   :param;
    has $x        :param :reader;

    method add_message ( $message ) {
        local $Text::Wrap::columns = $width;
        for ( Text::Wrap::wrap( '', '', $message->text ) ) {
            shift @{ $messages } while @{ $messages } >= $height;
            push @{ $messages }, Message->new( $_, $message->color );
        }
    }

    method messages { @{ $messages } }
}
